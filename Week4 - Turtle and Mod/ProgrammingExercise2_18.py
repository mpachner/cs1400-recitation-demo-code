# Programming Exercise 2.18 (Page 59), You have to scroll down a little past the start
# The problem asks us to prompt the user for an offset to GMT and then we
# print out the time at that offset. -7 is MST (Salt Lake City/Denver)

# Most of the problem is written out for you in the book (page 47) with confusing variable names
# Hopefully we code will be easier to follow

# Like turtle, time is a module that we import into our file
import time

# We use the .time() function on the time module to get the time in seconds
# e.g. 1238.403
currentTime = time.time()

# Because time.time() has milliseconds, we need to trim them. We can use the
# function int() to floor a number (int(4.3) => 4)
# time.time() starts it's time at January 1, 1970 GMT. This time is called the "Epoch"
secondsSinceEpoch = int(currentTime)

# Using our mod operator, we can extract the precise second by finding the seconds
# that remain from a minute. There are 60 seconds in a minute, so we mod 60
# We can only have a remainder of 0->59 on 60, so this is exactly what we want
currentSecond = secondsSinceEpoch % 60

# To calculate minutes, we need to change our seconds into minutes. We only want to
# Count full minutes, so we use integer divide instead of divide. We could alternatively
# Use normal divide and call int() on the answer.
minutesSinceEpoch = secondsSinceEpoch // 60  # Alternatively: minutesSinceEpoch = int(secondsSinceEpoch / 60)

# To get the current minute, we get the remainder of minutes in an hour. This is identical
# to how we got our current second. (60 minutes in an hour)
currentMinute = minutesSinceEpoch % 60

# To get total hours elapsed since the Epoch, we follow the same logic we used to get minutes
hoursSinceEpoch = minutesSinceEpoch // 60

# To get the current hour, we calculate the remainder of hours in a day. (24 hours in a day)
currentHour = hoursSinceEpoch % 24

# Our current hour is in GMT, we might want to display a different time zone, so we ask the user
# We can use int() instead of eval() here because they should only input integers
# -7 is GMT
offsetHours = int(input("Enter an offset to GMT: "))

# Print out the offset time. Add it to the current hour. This will print in 24-hour format
print("The time offset from GMT by", offsetHours, "is", currentHour + offsetHours, ":",
      currentMinute, ":", currentSecond)
