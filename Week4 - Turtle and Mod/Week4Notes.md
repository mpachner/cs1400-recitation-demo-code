# Week 4 - Turtle and Mod

## Problems:
- Case Study 2.14 (Page 52)
- Programming Exercise 2.18 (Page 59)

## Mod and integer divide

Mod and integer divide are common Computer Science mathematical operators. The mod operator is % and the integer divide 
operator is //.
```python
print(7 % 3)
print(7 // 3)
```

Think of these operations like elementary division. If we divide 7 by 3, it goes in 2 times, and has 1 remainder. (2*3 = 6, 6+1 = 7)
Integer divide gives us how many times the divisor goes into the dividend (In this case: 2). Mod gives us the remainder
(In this case: 1). 

## Turtle

You can see all turtle commands at: https://docs.python.org/3.3/library/turtle.html?highlight=turtle

Turtle is a module written by other programmers. In order to use turtle we have to import it into our project.
To do this, we use the "import keyword". By convention, we put all our imports we use at the start of our file.

```python
import turtle
```

Now we can use code inside the turtle module. We can access functions in the turtle module by using the dot operator.
Functions must have parenthesis, even if you don't write anything inside of them. The .done() function keeps the
turtle window open so we can view what was drawn.

```python
import turtle
turtle.done()
```

Turtle is a special module because it gives us the option to make our own turtle, or use their default turtle.
I would advise creating your own turtle. This will make your life easier in the future by giving you better error
messages and not having ghost turtles (These guys will appear in multi-file projects if you are not careful). To
create our own turtle, we have to use a turtle function called "Turtle()". I name my turtle Timmy. Gives him more character.
Do not name your turtle "turtle". The book does this and it makes everything weird.

```python
import turtle
timmy = turtle.Turtle()
```

Now that we have a turtle, we can make them draw for us. Our turtle will always draw by default. We can toggle if 
we want our turtle to draw or not using .penup() and .pendown(). We can tell our turtle to go places with .goto(x,y). Here 
is an example rectangle using these 3 functions.

```python
# Allow this file to use turtle commands
import turtle

# Create Timmy
timmy = turtle.Turtle()

# Disable Timmy's ability to draw until we are in the right spot
timmy.penup()

# Move to a spot to start our rectangle
timmy.goto(10,10)

# Allow timmy to draw again
timmy.pendown()

# Now for a rectangle
timmy.goto(10,20)
timmy.goto(20,20)
timmy.goto(20,10)
timmy.goto(10,20)

# Freeze all turtles. Note we use the turtle module, not timmy
turtle.done()

```

We can change the outside and inside color of shapes that we draw. To color our pen (outside) we can call
.color(<color>) where <color> is a color name ("blue","green","black", etc.)

```python
# Draws a blue line from (0,0) to (100,100)
import turtle
timmy = turtle.Turtle()
timmy.color("blue")
timmy.goto(100,100)
turtle.done()
```

To change the inside of a shape, we have to tell Timmy where a shape stops and starts, he will try to fill the inside.
The functions are .begin_fill() and .end_fill(). We choose our color the same way as the outside, .color()

```python
import turtle
timmy = turtle.Turtle()
timmy.color("blue")
# Tell timmy we are making a shape
timmy.begin_fill()
# Now for a rectangle
timmy.goto(10,0)
timmy.goto(10,10)
timmy.goto(0,10)
timmy.goto(0,0)
# Tell timmy we are done with the shape. He will not fill it in until we do so
timmy.end_fill()
turtle.done()
```

Turtles can also be given relative movements: .forward(length) .backward(length) .right(degree) .left(degree)
Turtles by default look at degree 0 (Directly to the right). You can tell a turtle to face a certain direction .setheading(degree)

```python
# Draw a Lambda
import turtle
timmy = turtle.Turtle()
# Point timmy up and left
timmy.setheading(120)
# Draw the main leg of the lambda
timmy.forward(100)
# Move to the midpoint of the lambda
timmy.backward(50)
# Tell timmy to rotate left (Relative to him)
timmy.left(120)
# Draw secondary leg of lambda
timmy.forward(50)

turtle.done()
```

Timmy takes a while to draw and ain't nobody got time for that. We can speed Timmy up by using .speed(100). Timmy can also
cover up details of our drawing. We can hide him by using .hideturtle(). If you miss him, use .showturtle() to bring him back.
Circles are hard to drawn with Timmy's given commands, so he comes with a built in circle function .circle(radius). Hide and 
speed up your turtles on your homework assignments.

```python
# Draw a speedy red circle
import turtle
timmy = turtle.Turtle()
# Make timmy fast
timmy.speed(100)
# Make timmy invisible
timmy.hideturtle()
# Make timmy draw in red
timmy.color("red")
# Mark the beginning of a shape
timmy.begin_fill()
# Draw the shape, we are making a circle with a radius of 100
timmy.circle(100)
# Mark the end of the shape
timmy.end_fill()

turtle.done()
```