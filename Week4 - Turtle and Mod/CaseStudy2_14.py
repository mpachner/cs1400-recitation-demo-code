# Case Study 2.14 (Page 52)
# The entire problem is written out for you in the book
# I have made some modifications to match my notes
import turtle

# Ask the user for two points. I would advise 0,0 and 100,100
x1, y1 = eval(input("Enter your first point (x1,x2): "))
x2, y2 = eval(input("Enter your second point (x1,x2): "))

# Distance formula for 2 dimensions. Pythagorean theorem
# Raising a number to (1/2) or (0.5) is the same as square rooting
distance = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5

print("The distance between the two points is", distance)

# This gives us a turtle we can use to draw with
# We will go over the specifics in later weeks
# I name my turtle timmy.
timmy = turtle.Turtle()

# Draw a line and label the points
timmy.penup()
timmy.goto(x1, y1)
timmy.pendown()
timmy.write("Point 1")
timmy.goto(x2, y2)
timmy.write("Point 2")

# Draw the midpoint of the line
# Midpoint formula
timmy.penup()
timmy.goto((x2 + x1) / 2, (y2 + y1) / 2)
timmy.write(distance)

# Note how we call turtle.done() on the module ("turtle"), not on our instance ("timmy")
# .done() freezes all turtles.
turtle.done()
