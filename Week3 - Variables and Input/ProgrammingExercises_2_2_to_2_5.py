# 2.2
# Calculate the area and volume of a cylinder
radius, length = eval(input("Enter the radius and length of a cylinder: "))
area = radius * radius * 3.14159
print("The area is", area)
print("The volume is", area * length)
print()

# 2.3
# Convert feet to meters
feet = eval(input("Enter a value for feet: "))
print(feet, "feet is", feet * 0.305, "meters")
print()

# 2.4
# Convert pounds to kilograms
pounds = eval(input("Enter a value in pounds: "))
print(pounds, "pounds is", pounds * 0.454, "kilograms")
print()

# 2.5
# Calculate the total cost of a bill after tip
subtotal, gratuityRate = eval(input("Enter the subtotal and a gratuity rate: "))
gratuity = subtotal * (gratuityRate / 100)
print("The gratuity is", gratuity, "and the total is", subtotal + gratuity)
