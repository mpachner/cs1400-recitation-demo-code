"""
# Week 3 - Programming exercises 2.2 -> 2.5 (Page 55)

## Variables/Naming Conventions

A variable is data that we can use again later. Define variables before
you use them. Python reads values from the top of the file to the bottom.
You can define as many variables as you want.

```python
foo = 7 # foo is a variable with a value of 7
print(foo*3) # this will print 21
```
Consider the following:

```python
# START OF BAD EXAMPLE
print(foo*3)
foo = 7
# END OF BAD EXAMPLE
```

That example does not work because foo is defined after it is being used.
Consider the next example:
```python
foo = 10
bar = 5 * foo
fooBar = foo * bar
```

This example works because each variable (foo,bar,fooBar) is defined before it is used.

Consider the following:
```python
# START OF BAD EXAMPLE
foo = 7
print("foo")
# END OF BAD EXAMPLE
```

This code runs, but does not print 7, it prints foo. To print a variable, you cannot put it in quotes.

Variable names should describe the value they hold. If you have a person's name stored in a variable,
you should call it <name> and not <x> or <n>. The more descriptive the variable name, the better.
Multi-word variable names are encouraged, and follow lowerCamelCase. (Do not separate the words by spaces!)

lowerCamelCase # First worst is lowercase, subsequent words have their first word capitalized

Say our name variable is not descriptive enough for our purposes, and we need a first name and a last name
variable.

Lower Camel Case: firstName, lastName

## User input. 
The user can tell us what they would like certain variables to be.
We can use the input() function to prompt the user for input.
Like the print statement, we can put words inside the parenthesis.
Prompts should indicate what values the users are putting in, and variable
names should match the data being stored

```python
# START OF BAD EXAMPLE # Better example uses eval()
name = input("What is your name: ") # This will pause the program and wait for the user
print("Your name is:", name) # This will not print until the user has pressed enter in the run window
# END OF BAD EXAMPLE
```

Because of reasons we will cover later, input() might not give you the expected behaviour.
Whenever you ask the user for input, surround the input() with eval(). Make sure you match
your parenthesis!

```python
name = eval(input("What is your name: ")) # This will pause the program and wait for the user
print("Your name is:", name) # This will not print until the user has pressed enter in the run window
```

Multiple variables can be assigned in one eval(input()). This is useful for reading in associated values,
such as coordinates. Multiple values need to be separated by commas, both by the user, and you.

```python
x, y = eval(input("Enter the starting coordinates:"))
# The user will enter in 2 values, separated by a comma, eg: 50,30
```

If the user does not enter enough values, or too many values, Python will throw an error.
