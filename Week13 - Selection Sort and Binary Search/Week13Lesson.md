# Selection sort
Fill out the selection sort and binary search. Algorithm descriptions are in the notes for this week

Starter code:
```python
import time 
import random 

def main(): 
    numList = []
    for i in range(10000): 
        numList.append(random.randint(0, 10000)) 
    start = time.time() 
    selectionSort(numList) 
    print(time.time() - start) 
    print(numList[:50]) # Print first 50 elements
    print(binarySearch(10,numList)) # Check if 10 is in the list

def selectionSort(inputList): # Returns None
    pass

def binarySearch(item,inputList): # Returns Boolean
    pass


main() 
```