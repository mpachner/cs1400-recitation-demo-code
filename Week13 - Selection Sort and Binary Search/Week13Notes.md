# Week 13 - Sorting and Searching

At this point you know a majority of Python syntax! Everything after this is learning how to code more efficiently.

## Sorting
Sorting is one of the most common Computer Science problems. We measure how good a sort is by how long the algorithm 
takes in terms of the length of the list. We will go over Bubble Sort and Selection Sort. These are N^2 algorithms, where 
N is the length of the list. There are better algorithms, but they are a bit more complicated.

### Bubble sort
Bubble sort is an incremental sort where we organize a list by a series of comparisons. We look at a pair of the list at
a time and sort those two elements. I'll walk through this one by creating an ascending list (Bigger elements at the end)
```python
# Step 1
#      T T     Compare index 0 and 1. 0 is bigger, so we swap
lst = [5,2,1,3]
# Step 2       
#        T T   Compare index 1 and 2. 1 is bigger, so we swap
lst = [2,5,1,3]
# Step 3       
#          T T Compare index 2 and 3. 2 is bigger, so we swap
lst = [2,1,5,3]
# Step 4 - At this point we have gone through our whole list, and the largest
# element is at the final index, so we start again       
#      T T     Compare index 0 and 1. 0 is bigger, so we swap
lst = [2,1,3,5]
# Step 5 -       
#        T T   Compare index 1 and 2. 2 is bigger, so we do not swap
lst = [1,2,3,5]
# Step 6+ - At this point, the list is sorted. We don't know that though, so 
# we would have to continue the algorithm N times to make sure all N elements are sorted
# This is where the N^2 complexity arrives. I'm not going to do the remaining steps because
# nothing will happen     
```

### Selection sort
Selection sort is an instantaneous sort where we organize a list by direct placement. We find the extreme element
and insert it at the correct spot. I'll walk through this one with the same list as bubble sort
```python
# Step 1
#      T     T  Biggest element is 5, it should be at the end, swap index 0 and 3
lst = [5,2,1,3]
# Step 2
#      T   T    Biggest remaining element is 3, it should be at the next end, swap index 0 and 2
lst = [3,2,1,5]
# Step 3
#        T      Biggest remaining element is 2, it should be at the next end, swap index 1 and 1
lst = [1,2,3,5]
# Step 4
#      T        Biggest remaining element is 1, it should be at the next end, swap index 0 and 0
lst = [1,2,3,5]
# There were N steps for our list of N elements, the problem is, finding the biggest element in a list requires us looking
# through the whole list. So each list, we have to scan N elements. N*N = N^2.
```

## Searching
Having a large list can make out "lookup" times long. Finding if the number 7 exists in a list of 10 million elements could 
take a while. If we have a large amount of searches to complete, this is an even bigger problem

### Linear search
Linear search is how most of us search a list. It's fine. We start at the beginning and look at each element to see if it
the one we are looking for. I'm going to look for the number 7.
```python
#      T  Index 0 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#         T  Index 1 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#           T  Index 2 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#             T  Index 3 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#               T  Index 4 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#                 T  Index 5 is not 7.
lst = [1,10,3,5,5,3,7,8,9,2]
#                   T  Index 6 is 7!!!
lst = [1,10,3,5,5,3,7,8,9,2]
# In python we can do linear search with the keyword "in"
print(7 in lst) # Prints True
``` 

### Binary search
Binary search is a clever way we can reduce how many elements we need to check, but requires a sorted list. It's a less
than, greater than search. I'm going to look for 6. I'll round down even list middles
```python
#      List is not sorted :(
lst = [1,10,3,5,5,3,7,8,9,2]
lst = sorted(lst) # hand wavy method that sorts list
lst = [1,2,3,3,5,5,7,8,9,10] # List is sorted!
#  min|        |guess      |max  We guess 6 is in the middle of our range, we get 5, so we know 6 is above that number
lst = [1,2,3,3,5,5,7,8,9,10]
#             min|   |     |max  We guess 6 is in the middle of our range, we get 8, so we know 6 is below that number
lst = [1,2,3,3,5,5,7,8,9,10]
#             min| |max        We guess the middle of our range, we get 5, so we know 6 is above
lst = [1,2,3,3,5,5,7,8,9,10]
#             min| |max        We guess the middle of our range, we get 7, so we know 6 is below
lst = [1,2,3,3,5,5,7,8,9,10]
# At this point we know 6 must be below our max and above our min, which is not possible, so 6 is not in the list
# binary search is log_2(N), we decrease how many elements we need to search by half every attempt. Binary search is great. 
``` 
