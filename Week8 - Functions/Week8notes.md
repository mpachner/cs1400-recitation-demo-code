# Week 8 - Functions

## Case study 6.14 (Page 199)
Create a set of functions for drawing primitive turtle objects
- Line
- Text
- Point
- Circle (Centered)
- Rectangle

Put these functions in a file called "UsefulTurtleFunctions.py"
You can them import them into a new file "main.py" (book uses "UseCustomTurtleFunctions.py")
```python
# In main.py
from UsefulTurtleFunctions import * # (* indicates all functions in the file)
# This import method allows us to call all functions directly
```

## Functions

Functions are chunks of code that we can use again. Functions can do anything that you've already learned and some more:
have loops, declare variables, print, input, if statements etc. A function is defined with the keyword ```def``` and a 
name followed by parenthesis.
I will use ```pass``` to indicate a code block. Remember pass is just a placeholder
```python
def myFunction(): # Defines a function named "myFunction". Remember to use lowerCamelCase
    pass # Function statements must be indented in, similar to loops and ifs
```
Functions are more useful when we pass information into them, and when they return information. To pass information in, 
put them in the parenthesis, comma separated. To return a value, we use the keyword ```return```
```python
def distance(x1,y1,x2,y2): # Euclidean distance between two points
    return ((y2-y1)**2 + (x2-x1)**2) ** 0.5 
```
To call a function, we have to have first defined it (white lie) in our file. We can then use the name to pass arguments
into the function, and if it returns something, use that value.
```python
def distance(x1,y1,x2,y2): # Euclidean distance between two points
    return ((y2-y1)**2 + (x2-x1)**2) ** 0.5 
dist1 = distance(0,0,3,4) # Sets dist1 => 5.0
print(dist1,distance(0,0,6,8)) # prints 5.0 10.0
```
If you do not put a return statement in a function, the function will return the special value ```None```. If you assign a 
variable to the return of the function, it will then be ```None```. This is probably not what you meant to do. Put a return
statement in your function or do not assign your functions return value.
```python
# BAD EXAMPLE
def printName(name):
    print("Welcome to the program,",name)

output = printName("Mason") +  printName("Joe")
print(output)
```
Output
```text
Welcome to the program, Mason
Welcome to the program, Joe
Traceback (most recent call last):
  File "C:/Users/Mason/Documents/USU/SP_20/Recitation/DemoCode/scratch.py", line 5, in <module>
    output = printName("Mason") +  printName("Joe")
TypeError: unsupported operand type(s) for +: 'NoneType' and 'NoneType'
```
Python complains because the function returns a ```None``` and it does not know how to add things of type ```None```

Solutions:
```python
def printName(name): # Return a string, strings can be added together
    return "Welcome to the program, " + name + "\n"

output = printName("Mason") +  printName("Joe")
print(output)
```
```python
def printName(name):
    print("Welcome to the program,",name)

printName("Mason") # Does not use the return type, just runs the function
printName("Joe")
```
Output of either:
```text
Welcome to the program, Mason
Welcome to the program, Joe
```
Default arguments can be assigned in a function declaration. This can make calling the function easier / more readable.
Use ```variable_name=value``` in the definition and the call.
```python
def doTheRoar(doIt=False):
    if doIt:
        print("Roar!")

doTheRoar() # Does nothing
doTheRoar() # Does nothing
doTheRoar() # Does nothing
doTheRoar(doIt=False) # Does nothing
doTheRoar(doIt=True) # prints Roar!
```
We can mix default arguments with normal arguments but all normal arguments MUST be before named arguments
```python
def drawCircle(x,y,radius=5,color="Black"): # Ok!
    pass
def drawRectangle(x,y,color="Black",width,height): # NOT OK! SYNTAX ERROR
    pass
```
