# Week 14 - Multidimensional lists
Lists are variables that can hold variables inside of them. This means lists can have lists inside of them. We call list-lists
multidimensional lists, where the dimension is how many inner lists they have
```python
oneDimensionalList = []
twoDimensionalList = [[]]
threeDimensionalList = [[[]]]
```
You can have as many dimensions as you want. I would advise keeping the dimensionality of your lists in the physical realm.
(dims <= 3). 20 dimensional lists are just confusing. I like 2 dimensional lists. I'll walk through an example of the start
of a 2D game, snake.

Snake is played on a discrete 2D board, for this example will be 30x40. 0 Represents an empty cell.
```python
BOARD_ROWS = 30
BOARD_COLS = 40
board = [] # Create a list to store rows
for i in range(BOARD_ROWS):
    row = [] # Create a list to store cells
    for j in range(BOARD_COLS):
        row.append(0)
    board.append(row)

# Or we could just list comp a list comp... :D
board = [[0 for _ in range(BOARD_COLS)] for _ in range(BOARD_ROWS)] 
```
Now we should pick a spot for our food and snake, and they probably shouldn't be the same square. 1 will be food, and 2 will be a snake

```python
import random
BOARD_ROWS = 30
BOARD_COLS = 40
board = [[0 for _ in range(BOARD_COLS)] for _ in range(BOARD_ROWS)] 
snakeRow = random.randint(0,BOARD_ROWS-1)
snakeCol = random.randint(0,BOARD_COLS-1)
foodRow = random.randint(0,BOARD_ROWS-1)
foodCol = random.randint(0,BOARD_COLS-1)
while snakeCol == foodCol and snakeRow == foodRow:
    foodRow = random.randint(0,BOARD_ROWS-1)
    foodCol = random.randint(0,BOARD_COLS-1)

# we use the [] operator to get an element, the element is a list, so we have to do it twice!
# Keep in mind, the outer and inner list have different lengths, so we have to watch our indices.
board[snakeRow][snakeCol] = 2
board[foodRow][foodCol] = 1
```
Notice that we just had a lot of duplicate code, and if we wanted to start with 3 food spots, we would have to repeat even more,
so I'm going to make a function that puts in a element at an empty spot.
```python
import random
BOARD_ROWS = 30
BOARD_COLS = 40
board = [[0 for _ in range(BOARD_COLS)] for _ in range(BOARD_ROWS)] 
def insertAtEmpty(board, toInsert):
    row = random.randint(0,len(board)-1) # length of board is how many rows
    col = random.randint(0,len(board[0])-1) # length of first row is how many columns
    while board[row][col] != 0:
        row = random.randint(0,len(board)-1)
        col = random.randint(0,len(board[0])-1)
    board[row][col] = toInsert
for _ in range(3): # create 3 foods
    insertAtEmpty(board,1) # create a food
insertAtEmpty(board,2) # create 1 snake spot
```
Now lets print it out so we can see our board in ASCII. 0's will be _ for readability
```python
import random

BOARD_ROWS = 30
BOARD_COLS = 40
board = [[0 for _ in range(BOARD_COLS)] for _ in range(BOARD_ROWS)]


def insertAtEmpty(board, toInsert):
    row = random.randint(0, len(board) - 1)  # length of board is how many rows
    col = random.randint(0, len(board[0]) - 1)  # length of first row is how many columns
    while board[row][col] != 0:
        row = random.randint(0, len(board) - 1)
        col = random.randint(0, len(board[0]) - 1)
    board[row][col] = toInsert


def printBoard(board):
    for row in board:  # by item loop (Foreach)
        for cell in row:  # by item loop (Foreach)
            print(str(cell if cell != 0 else "_"), end=" ")
        print()


for _ in range(3):  # create 3 foods
    insertAtEmpty(board, 1)  # create a food
insertAtEmpty(board, 2)  # create 1 snake spot
printBoard(board)
```
Output (Randomized)
```text
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 2 _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ 1 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 1 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 1 _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
```

If you wanted to continue the game, I would advise making a class for food and snake. That way you could keep the state of
the snake's location and move it easier. Let's pretend that we have a Snake class and want to move it and see if it has crashed

```python
directions = ["UP","DOWN","LEFT","RIGHT"]
def getValidSpot(board):
    return [0,0] # Return the valid spot in x,y form. 0,0 for example
class Snake:
    def __init__(self,board):
        self.__board = board
        self.__head = getValidSpot(board)
        self.__cells = [self.__head] # store the snake as a list of lists
        self.__toGrow = 0 # Record how many spots we need to extend
        self.__alive = True            

    # Keep in mind 0,0 is the top left of our grid! This is common in 2D
    # the head of the snake if the front
    def move(self,direction):
        newHead = [self.__head[0],self.__head[1]]
        if direction == "UP":
            newHead[1] -= 1 # Up is negative!
        if direction == "DOWN":
            newHead[1] += 1 # Down is positive!
        if direction == "LEFT":
            newHead[0] -= 1
        if direction == "RIGHT":
            newHead[0] += 1  
        # bounds check, make sure we are still in the board
        if newHead[1] < 0 or newHead[0] < 0 or newHead[1] >= len(self.__board[0]) or newHead[0] >= len(self.__board):
            self.__alive = False
            return
        if self.__board[newHead[1]][newHead[0]] == 2:
            self.__alive = False
            return # more snake. bad.
        # tell the board where we are
        self.__board[newHead[1]][newHead[0]] = 2
        # add the head to the cells
        self.__cells.insert(0,newHead)

        # Check if we ate
        if self.__board[newHead[1]][newHead[0]] == 1:
            self.__toGrow += 1 # food! yum!
            newFoodSpot = getValidSpot(self.__board)
            self.__board[newFoodSpot[1]][newFoodSpot[0]] = 1

        if self.__toGrow > 0:
            self.__toGrow =- 1 # yum yum! don't shrink
        else:
            self.__board[newHead[1]][newHead[0]] = 2
            self.__cells.pop() # remove the last cell where we were
```
Kind of complicated. Let's see it all together
```python
import random


def printBoard(board):
    for row in board:  # by item loop (Foreach)
        for cell in row:  # by item loop (Foreach)
            print(str(cell if cell != 0 else "_"), end=" ")
        print()


def getValidSpot(board):
    row = random.randint(0, len(board) - 1)  # length of board is how many rows
    col = random.randint(0, len(board[0]) - 1)  # length of first row is how many columns
    while board[row][col] != 0:
        row = random.randint(0, len(board) - 1)
        col = random.randint(0, len(board[0]) - 1)
    return [row, col]  # Return the valid spot in x,y form. 0,0 for example


def insertAtEmpty(board, toInsert):
    goodSpot = getValidSpot(board)
    board[goodSpot[0]][goodSpot[1]] = toInsert


class Snake:
    def __init__(self, board):
        self.__board = board
        self.__cells = [getValidSpot(board)]  # store the snake as a list of lists
        board[self.__cells[0][0]][self.__cells[0][1]] = 2
        self.__toGrow = 0  # Record how many spots we need to extend
        self.__alive = True

        # Keep in mind 0,0 is the top left of our grid! This is common in 2D

    # the head of the snake is the front
    def move(self, direction):
        newHead = [self.__cells[0][0], self.__cells[0][1]]
        if direction == "u":
            newHead[0] -= 1  # Up is negative!
        elif direction == "d":
            newHead[0] += 1  # Down is positive!
        elif direction == "l":
            newHead[1] -= 1
        elif direction == "r":
            newHead[1] += 1
        else:
            return
            # bounds check, make sure we are still in the board
        if newHead[1] < 0 or newHead[0] < 0 or newHead[1] >= len(self.__board[0]) or newHead[0] >= len(self.__board):
            self.__alive = False
            return
        if self.__board[newHead[0]][newHead[1]] == 2:
            self.__alive = False
            return  # more snake. bad.

        # Check if we ate
        ate = self.__board[newHead[0]][newHead[1]] == 1
        # tell the board where we are
        self.__board[newHead[0]][newHead[1]] = 2
        # add the head to the cells
        self.__cells.insert(0, newHead)
        if self.__toGrow > 0:
            self.__toGrow -= 1  # yum yum! don't shrink
        else:
            self.__board[newHead[0]][newHead[1]] = 2
            spotToReset = self.__cells.pop()  # remove the last cell where we were
            self.__board[spotToReset[0]][spotToReset[1]] = 0
        if ate:
            self.__toGrow += 1  # food! yum!
            insertAtEmpty(self.__board, 1)

    def isAlive(self):
        return self.__alive


BOARD_ROWS = 15
BOARD_COLS = 20
board = [[0 for _ in range(BOARD_COLS)] for _ in range(BOARD_ROWS)]

for _ in range(3):  # create 3 foods
    insertAtEmpty(board, 1)  # create a food
snake = Snake(board)

while snake.isAlive():
    printBoard(board)
    snake.move(input("Enter a direction(u,d,l,r): "))

print("ded")

```
This works, with a couple of bugs, you can run into your tail and it does not check if the board is full to insert a new food. But it works for demo purposes