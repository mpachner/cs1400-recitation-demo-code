# Classes
Classes allow us to store groups of variables and functions together under a single source.
They take a bit of effort to setup, but make our lives easier in the long run.
Turtle is a class that you are familiar with.

Classes follow the UpperCamelCase naming convention and are defined as such:
```python
class MyClass:
    pass
```
A class is a template for data, and is a type, just like int, float, string, etc. For the "MyClass" example, the
type of "MyClass" is MyClass. 

We can create an instance of the class by defining the ```__init__()``` method. ```__init__()``` is a special method because it has a 
beginning and ending __ (Double underscore, 'dunder', like Dunder Mifflin). You'll learn more special methods later. The ```__init__()```
method is called the Constructor because it is what we call to construct an instance of the class.

I am calling these functions "Methods" because they belong to a class. That is the only difference.
I'm going to define a class called Vector, which is a 2-Dimensional Vector, and make an instance of the class

```python
class Vector:
    def __init__(self,x,y): # This is your constructor
        print("I am a vector with x=",x,"and y=",y)

# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(5,5) 
```
A couple things are weird here. First off, the first argument into the ```__init__()``` function is self, and then x,y, but we only
call with 2 arguments. 

This is how methods are defined. As a method is a function of a class, python passes the class in as the first argument. Python
does this behind the scenes for us so we don't have to. Make sure you have self as the first argument for any method so you 
can access attributes and functions of that object in the method. eg (self.x)

Also, we aren't calling the ```__init__()``` function, but the classname?

All special methods have special ways of calling them, not directly by their name. ```__init__()``` is called by using the
classname. When you call the constructor, use the classname, not ```__init__()```

Currently, our Vector doesn't do much. It would be nice to store the x and y variables inside the class for use later.
remember that 'self' is the instance of the class, so we should store our variables there. That way we can ask the vector
for them later
```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y

# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(5,3)
print("I have made a vector with x=",vectorA.x,"and y=",vectorA.y)
```
Cool! Now our object can store information and we get it back by using the dot operator, which should be familiar from turtle.
Using our variables, we can now inquire other information about the Vector
```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def getMagnitude(self): # Returns the magnitude of the vector
        return pow(pow(self.x,2) + pow(self.y,2),0.5)

# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(4,3)
print("I have made a vector with x=",vectorA.x,"and y=",vectorA.y)
print("The magnitude of my vector is", vectorA.getMagnitude())
```

Note that again, we do not pass 'self' into our method when we call it, but we do when we define it.
We call our methods and variables on the instance (vectorA) of the class (Vector).
One instance (a.k.a. object) is cool, but we want a lot of Vectors. If we had 2 vectors we could do cool things
like dot products and cross products. (If you haven't taken linear algebra yet, believe me, these are super cool).


Luckily, we can already make as many Vectors as we want. We just have to create them.
 
```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def getMagnitude(self): # Returns the magnitude of the vector
        return pow(pow(self.x,2) + pow(self.y,2),0.5)

# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(4,3)
print("I have made a vector with x=",vectorA.x,"and y=",vectorA.y)
print("The magnitude of my vector is", vectorA.getMagnitude())

vectorB = Vector(10,2)
print("I have made a vector with x=",vectorB.x,"and y=",vectorB.y)
print("The magnitude of my vector is", vectorB.getMagnitude())

vectorC = Vector(-34,78)
print("I have made a vector with x=",vectorC.x,"and y=",vectorC.y)
print("The magnitude of my vector is", vectorC.getMagnitude())
```

That got a bit messy, so lets clean it up a bit. We report the stats of each vector, so we can just make a method for that.

```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def getMagnitude(self): # Returns the magnitude of the vector
        return pow(pow(self.x,2) + pow(self.y,2),0.5)

    def report(self): # Doesn't return anything
        print("I have made a vector with x=",self.x,"and y=",self.y)
        print("The magnitude of my vector is", self.getMagnitude())

# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(4,3)
vectorA.report()
vectorB = Vector(10,2)
vectorB.report()
vectorC = Vector(-34,78)
vectorC.report()
```
Exact same output, a lot more maintainable (If we want to change what we output, we only have to change one place) and 
readable (Less bloby).

Now I'll add in the dot and cross product, as well as a subtract and add. It's okay if you don't understand the math, just know you need two Vectors to 
do the operations

```python
class Vector:
    def __init__(self, x, y):
        self.x = x  # Float
        self.y = y  # Float

    def getMagnitude(self):  # Returns float
        return pow(pow(self.x, 2) + pow(self.y, 2), 0.5)

    def report(self):  # returns None
        print("I have made a vector with x=", self.x, "and y=", self.y)
        print("The magnitude of my vector is", self.getMagnitude())

    def asString(self):  # returns String
        return "{" + str(self.x) + "," + str(self.y) + "}"

    def add(self, other):  # Returns None
        self.x += other.x  # other needs to be a Vector as well
        self.y += other.y

    def subtract(self, other):  # Returns None
        self.x -= other.x
        self.y -= other.y

    def dot(self, other):  # Returns float
        return self.x * other.x + self.y * other.y

    def cross(self, other):  # Returns float
        return self.x * other.y - self.y * other.x


# The type of vectorA is 'Vector' 
# vectorA is an instance of Vector
vectorA = Vector(4, 3)
vectorB = Vector(10, 2)
print(vectorA.asString())  # initial state of vectorA
print(vectorA.dot(vectorB))  # non-mutating operation, returns a new value, does not change old values
print(vectorA.cross(vectorB))  # non-mutating operation
vectorA.add(vectorB)  # mutating operation! Changes x and y
print(vectorA.asString())  # will not be the same as initial state
vectorA.subtract(vectorB)  # mutating operation, should move vector back to initial location
print(vectorA.asString()) # should be ~ initial location again
```
Output
```text
{4,3}
46
-22
{14,5}
{4,3}
```
I also added in a 'asString()' function on my vector for easy printing. If you just print out a class instance, python will 
print out ('instance of class 'Vector''), which isn't too useful.

## Access modifiers
### public and private variables and methods

In the vector example, every variable and method was public. This means the methods and variables can be used anywhere
by anyone and do anything. For simple examples, this is fine. It's easy to keep track of what is going on and who is doing what.
However, this is bad coding practice for large projects. You can easily lose a case of a modification in your millions of 
lines of code. To combat this problem, we have different access levels on variables. For now, all you need to know is public and
private. A public variable/method may be called or modified from anywhere, a private variable can only be called inside of
the class where it is defined.

Private methods and attributes are defined by dunder '__'

Examples:
```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y
class Circle:
    def __init__(self,radius):
        self.__radius = radius

circle = Circle(4)
vector = Vector(4,10)
print(circle.__radius) # This throws an error, __radius is not an attribute of Circle
print(vector.x) # This works!
EVIL_VALUE = -5
circle.__radius = EVIL_VALUE # This throws an error, __radius is not an attribute of Circle
vector.y = EVIL_VALUE # This works.... hmm
```

Odds are you won't have an evil hacker trying to modify your classes, odds are you will be the person modifying things
you shouldn't. In order to protect our variables we use methods called getters and setters.

```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y
class Circle:
    def __init__(self,radius):
        self.__radius = radius

    def getRadius(self):
        return self.__radius

    def setRadius(self,newRadius):
        self.__radius = newRadius

circle = Circle(4)
vector = Vector(4,10)
print(circle.getRadius()) # This works!
print(vector.x) # This works!
EVIL_VALUE = -5
circle.setRadius(EVIL_VALUE) # This works... hmm
vector.y = EVIL_VALUE # This works.... hmm
```

So it looks like the evil value got in and now we have a negative radius. Luckily we can fix this by putting some error
handling on our setter function

```python
class Circle:
    def __init__(self,radius):
        self.__radius = radius

    def getRadius(self):
        return self.__radius

    def setRadius(self,newRadius):
        if newRadius <= 0:
            print("No negative radii allowed. Begone!")
            return
        self.__radius = newRadius

circle = Circle(4)
print(circle.getRadius()) # This works!
EVIL_VALUE = -5
circle.setRadius(EVIL_VALUE) # This print our error message and protects our circle
```
