# UML
Universal markup language: how to describe a class

UML is a method of documenting classes to show what attributes and methods exist

Lets look at our vector class from last week, modified to use private variables and a private method

```python
class Vector:
    def __init__(self, x, y):
        self.__x = x  # Float
        self.__y = y  # Float

    def __getMagnitude(self):  # Returns float
        return pow(pow(self.__x, 2) + pow(self.__y, 2), 0.5)

    def report(self):  # returns None
        print("I have made a vector with x=", self.__x, "and y=", self.__y)
        print("The magnitude of my vector is", self.__getMagnitude())

    def asString(self):  # returns String
        return "{" + str(self.__x) + "," + str(self.__y) + "}"

    def add(self, other):  # Returns None
        self.__x += other.__x  # other needs to be a Vector as well
        self.__y += other.__y

    def subtract(self, other):  # Returns None
        self.__x -= other.__x
        self.__y -= other.__y

    def dot(self, other):  # Returns float
        return self.__x * other.__x + self.__y * other.__y

    def cross(self, other):  # Returns float
        return self.__x * other.__y - self.__y * other.__x

    def getX(self):
        return self.__x
    
    def getY(self):
        return self.__y
```

The respective UML of the Vector class would look something like this:
```text
==========================================
Vector
==========================================
-x : float
-y : float
==========================================
+Vector(x : float, y : float) : Vector
-getMagnitude() : float
+report() : None
+asString() : String
+add(other : Vector) : None
+subtract(other : Vector) : None
+dot(other : Vector) : float
+cross(other : Vector) : float
+getX() : float
+getY() : float
==========================================
```

We head our UML with the class name
We then have two sections, attributes and methods
In the attributes section we have the variables of the class
In the methods section... we define the methods...

A method or variable should begin with a +(public) or -(private)
each variable, parameter, and function should end with a : type

Instead of use ```__init__()``` for the constructor, we use the class name, similar to how we would call the constructor.


# The index operator []
The index operator is a powerful tool to let us access parts of a collection.
For now, we will just use them with strings. In python we use 0-based indices
(First element is at 0, second element is at 1) 
```python
print("Hello World"[0]) # Outputs H
print("Hello World"[1]) # Outputs e
```
We can also access backwards with a index operator. 
```python
print("Hello World"[-0]) # Still outputs H, -0 == 0
print("Hello World"[-1]) # Outputs d
print("Hello World"[-2]) # Outputs l
```
Index operators are useful for loops where we go through the string
The length of ```hello``` is 5, so range will go from [0,5) 
```python
hello = "Hello"
for i in range(len(hello)):
    print(hello[i])
```
Output
```text
H
e
l
l
o
```
The index operator can be used incorrectly. Make sure the number you put inside the [] is not too large or too small
 ```python
hello = "Hello World"
print(hello[11]) # 'Hello World' is 11 characters, but the eleventh index would be the 12th charachter...
print(hello[len(hello)]) # exact same problem.
```
This will throw an error saying "String index out of range". This is because your index (thing inside the []) is not 
in the range of the string [0-10]
```python
hello = "Hello World"
print(hello[10]) # prints 'd'
print(hello[len(hello)-1]) # prints 'd'
```
