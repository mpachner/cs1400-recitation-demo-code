# Online lecture notes 3/25/2020
Practice with classes and the index operator [].

The assignment this week is to create a Palindrome class and a menu to use it.

Palindromes are words that are spelled the same way backwards and forwards.
```kayak``` and ```hannah``` are example palindromes.

The Palindrome class has to follow these requirements:

- There should be a private member variable named 'word' (Add the __)
- There should be a getter and setter for word
- There should be a method called isPalindrome() which returns True/False if the word is a palindrome

The main function has to follow these requirements:

- The Palindrome constructor (```__init__()```  or Palindrome()) is only called once. (Do not put it in a loop!)
- The user can enter new words to see if they are palindromes
- The user can choose to exit the program

You should use a loop to ask for new words, just make sure to use the setter in the loop, and create the Palindrome instance
before the loop.



Example run:
```text
Enter a palindrome: Mason
Mason is not a palindrome
Would you like to enter another word? (Yes/No): Yes
Enter a palindrome: 4g oro g4
4g oro g4 is a palindrome
Would you like to enter another word? (Yes/No): No
Bye!
```


