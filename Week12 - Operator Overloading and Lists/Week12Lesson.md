Here is what I went over during the lesson.
```python
class Rational:
    # Most reduced form of fraction
    def __init__(self, numerator=0, denominator=1):
        divisor = gcd(abs(numerator), abs(denominator))
        self.__numerator = (1 if denominator > 0 else -1) \
                           * int(numerator / divisor)
        self.__denominator = int(abs(denominator) / divisor)

    # 1/2 + 2/3 == 7/6
    # 1*3/2*3 + 2*2/3*2
    def __add__(self, other):  # -> Rational:
        return Rational(self.__numerator * other.__denominator + other.__numerator * self.__denominator,
                        self.__denominator * other.__denominator)

    # 1/2 - 2/3 == -1/6
    def __sub__(self, other):  # -> Rational:
        return Rational(self.__numerator * other.__denominator - other.__numerator * self.__denominator,
                        self.__denominator * other.__denominator)

    # 1/2 * 2/3 == 2/6
    def __mul__(self, other):  # -> Rational:
        return Rational(self.__numerator * other.__numerator, self.__denominator * other.__denominator)

    # 1/2 / 2/3 == 3/4
    def __truediv__(self, other):  # -> Rational:
        return Rational(self.__numerator * other.__denominator, self.__denominator * other.__numerator)

    def __str__(self):
        return "" + str(self.__numerator) + "/" + str(self.__denominator)

    def __float__(self):
        return self.__numerator / self.__denominator

    def __le__(self, other):
        return float(self) <= float(other)

    def __ge__(self, other):
        return float(self) >= float(other)

    def __lt__(self, other):
        return float(self) < float(other)

    def __gt__(self, other):
        return float(self) > float(other)

    def __eq__(self, other):
        return float(self) == float(other)

    def __ne__(self, other):
        return float(self) != float(other)

    def getString(self):
        return "" + str(self.__numerator) + "/" + str(self.__denominator)

    def __int__(self):
        return self.__numerator // self.__denominator

    def __getitem__(self, item):
        if item == 0:
            return self.__numerator
        else:
            return self.__denominator


def gcd(num1, num2):
    biggestDivisor = 1
    for i in range(2, max(num1, num2)):
        if num1 % i == 0 and num2 % i == 0:
            if i > biggestDivisor:
                biggestDivisor = i
    return biggestDivisor


# r1 = Rational(1, 2)
# r2 = Rational(2, 3)
#
# print((r1 * r2).getString())
# print(str(r1 * r2))
# print(r1 / r2)
# print(r1 + r2)
# print(r1 - r2)
#
# print(r1, ">", r2, "is", r1 > r2)
# print(r1, ">=", r2, "is", r1 >= r2)
# print(r1, "<", r2, "is", r1 < r2)
# print(r1, "<=", r2, "is", r1 <= r2)
# print(r1, "==", r2, "is", r1 == r2)
# print(r1, "!=", r2, "is", r1 != r2)
#
# print(r1[0])
# print(r1[1])
#
# print(Rational(1, 12) * Rational(4, 3))


listExample = []

listExample = [1, 32, 2]

listExample = [30, 1, 12, 14, 10, 0]

print(len(listExample))
print(listExample[0])
print(listExample[len(listExample) - 1])
print(listExample[-1])
print(listExample[2])  # 12
print(listExample[-2])  # 10
import random

listExample = [30, 1, 2, 1, 0]

listExample.append(40)  # [30, 1, 2, 1, 0, 40]
listExample.insert(1, 43)  # [30, 43, 1, 2, 1, 0, 40]
listExample.extend([1, 43])  # [30, 43, 1, 2, 1, 0, 40, 1 , 43]
listExample.remove(1)  # [30, 43, 2, 1, 0, 40, 1, 43]
listExample.pop(1)  # [30, 2, 1, 0, 40, 1, 43]
listExample.pop()  # [30, 2, 1, 0, 40, 1]
listExample.sort()  # [0,1,1,2,30,40]
listExample.reverse()  # [40,30,2,1,1,0]
random.shuffle(listExample)
print(listExample)

listExample = [30, 1, 2, 1, 0]
listExample.index(1)
listExample.count(1)
len(listExample)
max(listExample)
min(listExample)
sum(listExample)

list1 = [30, 1, 2, 1, 0]
list2 = [1, 21, 13]

print(list2 * 2)
print(list1[1:3])  # [1,2]

list1 = [30, 1, 2, 1, 0]

print([item for item in list1])

list2 = []
for item in list1:
    list2.append(item)
print(list2)

print([x for x in list1 if x > 1])
print([x for x in range(0, 10, 2)])  # 0,2,4,6,8
print([x for x in range(10, 0, -2)])  # 10,8,6,4,2

list1 = [30, 1, 2, 1, 0]
list2 = [1, 21, 13]

print(list1 < list2)
print(list1 <= list2)
print(list1 == list2)
print(list1 != list2)
print(list1 > list2)
print(list1 >= list2)

# ["Mason", 4, 6] ok
# len = 3 ok
# [4,4,4,4,4] ok
# [4,2,7,6][0] => 4 ok

# Primitive type
x = 4
y = x  # y = 4.

# Reference types. BIG
# Orbian1 = Orbian2.
# ALIASING
# list -> other list
list1 = [1, 43]
list2 = list1
list1[0] = 22

print(list1)
print(list2)


def addItem(lst, item):
    lst.append(item)


def add5(item):
    item += 5


lst1 = [7]
addItem(lst1, 5)
print(lst1)

x = 6
add5(x)
print(x)

list1 = [1, 43]
list2 = [x for x in list1]
list1[0] = 22

print(list1)  # [22,43]
print(list2)  # [1,43]
print([x for x in "String"])
print("abbbabba".split('a'))

booleans = [False for _ in range(100)]

booleans[-1] = 5.5

print(sum(booleans[0:2]))

# 0 1 2 3 4
# False == 0
# True == 1
print(sum(booleans[0:5]))
print(min(booleans))

print(booleans[random.randint(0, 100 - 1)])




```