# Week 12 - Operator Overloading and Lists

## Lists
Lists are ways we can collect items together with an order. Lists can have from 0 to infinite items.
In python, there are 2 easy ways to create an empty list. Lists are just like any other variable.
```python
exampleList1 = []  # I like this method more and will use this
exampleList2 = list()
```
We can record data in lists that we can use later. Knowing the length of the list, we can iterate through the list with 
for and range.
```python
exampleList = [1,2,3,4,5] # creates a list with elements 1,2,3,4,5
for i in range(len(exampleList)): # Len returns the length of the list
    print(exampleList[i])
```
Output
```text
1
2
3
4
5
```
Just like strings, we can use the [] operator to access elements inside of the list. We also need to make sure that the
index is the range of the list.
```python
lst = [324,236,1324,60435,185,3420] # length is 6
print(lst[6]) # too far! this is an error
print(lst[len(lst)-1]) # This is the last element
print(lst[-1]) # This is also the last element
```
There are a lot of methods on list that we can use, the most useful one is `append`, which adds an element to an existing list
```python
lst = []
for i in range(100):
    lst.append(i**2)
print(lst)
```
output
```text
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961, 1024, 1089, 1156, 1225, 1296, 1369, 1444, 1521, 1600, 1681,...]
```
We can use lists to store large amounts of data that would be hard to create individual variables for.

Lists are "passed by reference" into functions because of how big they could be. This means the list used in a function is the 
same list that you pass in.

```python
def addNum(num):
    num+=1

def addListNum(lst):
    for i in range(len(lst)): # add 1 to all elements in the list
        lst[i] += 1
x = 1
addNum(x)
print(x)
y = [1]
addListNum(y)
print(y)
```
Output
```text
1
[2]
```
Notice how the value of x didn't change, but the value of y, our list, did. Similarly, just assigning new names to the list
is just the same list
```python
x = 1
y = x
y += 1
print(x)
a = [1]
b = a
b[0] += 1
print(a)
```
Output
```text
1
[2]
```

Lists can also be directly iterated into, making for loops even more useful.

```python
lst = [5,10,15]
for item in lst:
    print(item)
```
Output
```text
5
10
15
```
You can store anything in a list, Objects, strings, floats, ints... more lists!

There is a reduced python syntax for initializing a list called "list comprehension". I really enjoy list comprehension
and will use it whenever I can.

```python
lst = [i for i in range(10)] # => [0,1,2,3,4,5,6,7,8,9]
lst = [i**2 for i in range(5)] # => [0,1,4,9,16]
lst = [i for i in range(10) if i%2 == 0] # => [0,2,4,6,8]
```

You can also slice lists just like strings.

```python
lst = [i for i in range(10)][2:-2] # => [2,3,4,5,6,7]
lst = [i**2 for i in range(5)][1:] # => [1,4,9,16]
lst = [i for i in range(10) if i%2 == 0][::-1] # => [8,6,4,2,0] Reverse!
```

## Operator overloading
Operator overloading is a way that we can use python syntax on our objects instead of creating our own method system.
Lets take a look at the Vector class again:
```python
class Vector:
    def __init__(self, x, y):
        self.__x = x  # Float
        self.__y = y  # Float

    def __getMagnitude(self):  # Returns float
        return pow(pow(self.__x, 2) + pow(self.__y, 2), 0.5)

    def report(self):  # returns None
        print("I have made a vector with x=", self.__x, "and y=", self.__y)
        print("The magnitude of my vector is", self.__getMagnitude())

    def asString(self):  # returns String
        return "{" + str(self.__x) + "," + str(self.__y) + "}"

    def add(self, other):  # Returns None
        self.__x += other.__x  # other needs to be a Vector as well
        self.__y += other.__y

    def subtract(self, other):  # Returns None
        self.__x -= other.__x
        self.__y -= other.__y

    def dot(self, other):  # Returns float
        return self.__x * other.__x + self.__y * other.__y

    def cross(self, other):  # Returns float
        return self.__x * other.__y - self.__y * other.__x

    def getX(self):
        return self.__x
    
    def getY(self):
        return self.__y
```
Inside of here we have 3 methods we could easily turn into overloads. You just have to find an
appropriate operator. For example, add() could be the + operator, subtract could be -, and asString could be str(). 
Overloaded methods are marked with a dunder at the beginning and end of the method name, just like `__init__()`. +'s method
is `__add__()`, -'s is `__sub__()`, str()'s is `__str__()`. Most of them are reasonably named.
```python
class Vector:
    def __init__(self, x, y):
        self.__x = x  # Float
        self.__y = y  # Float

    def __getMagnitude(self):  # Returns float
        return pow(pow(self.__x, 2) + pow(self.__y, 2), 0.5)

    def report(self):  # returns None
        print("I have made a vector with x=", self.__x, "and y=", self.__y)
        print("The magnitude of my vector is", self.__getMagnitude())

    def __str__(self):  # returns String
        return "{" + str(self.__x) + "," + str(self.__y) + "}"

    def __add__(self, other):  # Returns Vector
        return Vector(self.__x + other.__x,
                      self.__y + other.__y)

    def __sub__(self, other):  # Returns Vector
        return Vector(self.__x - other.__x,
                      self.__y - other.__y)

    def dot(self, other):  # Returns float
        return self.__x * other.__x + self.__y * other.__y

    def cross(self, other):  # Returns float
        return self.__x * other.__y - self.__y * other.__x

    def getX(self):
        return self.__x
    
    def getY(self):
        return self.__y

a = Vector(0,0)
b = Vector(1,4)
c = a + b # This works!
d = a - b # This works!
print(a) # This prints our __str__ method!
print(b)
print(c)
print(d)
```
The class is essentially the same, the only difference is how we can use it now.

Try to only use operator overloading when the operator makes sense for the class. Don't made an `__add__` overload for
a Chair class. + is not a normal thing we do between two chairs. 

Also make note of what inputs an operator takes. + is a binary operator that returns the same type as the arguments, as such
the `__add__()` method takes 2 Vectors (self is one), and returns a Vector. == is an operator that takes two items, but 
returns a boolean. Make sure you understand the operator before you try to overload it.
The Lesson for this week goes over more operator overloading examples.
