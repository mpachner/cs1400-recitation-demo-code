# Week 2 - Programming exercises 1.3 -> 1.11 (Page 27)

## Print Statements. 
Print statements must be all lower case and have matching parenthesis.
Inside of the print statement you can put words or numbers. Words must be surrounded in quotes
and numbers cannot be in quotes unless you want them to be treated as a word.
You can use either single quotes ' or double quotes ", just make sure they match.

```python
print("Hello, World")
```

This prints to the console:
```text
Hello, World
```

```python
print(7)
```

This prints to the console:
```text
7
```

Numbers can also be mathematical expressions. Python follows order of operations. Extra parenthesis
are encouraged for your sake.

```python
print((7 + 3)/5)
```

This prints to the console:
```text
2
```
If you surround the math with quotes
```python
print("(7 + 3)/5")
```
This prints to the console:
```text
(7 + 3)/5
```

Multiplication is not implicit in Python, 4(7) is undefined. The expression must be 4*7.

## PyCharm
PyCharm has a lot of features and we will go over a small fraction of them throughout
the semester.

Running a file. Right-click the file (Where you are typing) and select
"Run" from the drop-down menu. (This also makes sure you are running the file you think you should
be running)

Renaming. To rename a file, right-click it and select "Refactor" and then "Rename".
