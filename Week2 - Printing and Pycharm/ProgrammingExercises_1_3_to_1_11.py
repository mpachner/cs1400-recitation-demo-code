# Week 2 - Programming exercises 1.3 -> 1.11 (Page 27)

# Print out a large FUN to console
print('# 1.3')
print()

print('FFFFFFF    U     U   NN    NN')
print('FF         U     U   NNN   NN')
print('FFFFFFF    U     U   NN N  NN')
print('FF          U   U    NN  N NN')
print('FF           UUU     NN   NNN')

# Print out a table to console
print()
print('# 1.4')
print()

print('a     a^2    a^3')
print('1     1      1')
print('2     4      8')
print('3     9      27')
print('4     16     64')

# Calculate a mathematical formula. Use as many parenthesis as you wish. Python follows standard order of operations
print()
print('# 1.5')
print()

print(((9 * 4.5) - (2.5 * 3)) / (45.5 - 3.5))

# Calculate the sum of a series
print()
print('# 1.6')
print()

print(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9)

# Calculate the sum of series to estimate PI
print()
print('# 1.7')
print()

print(4 * (1 - (1 / 3) + (1 / 5) - (1 / 7) + (1 / 9) - (1 / 11)))
print(4 * (1 - (1 / 3) + (1 / 5) - (1 / 7) + (1 / 9) - (1 / 11) + (1 / 13) - (1 / 15)))

# Calculate the area and diameter of a circle
print()
print('# 1.8')
print()

print(5.5 * 5.5 * 3.14)
print(2 * 5.5 * 3.14)

# Calculate the area and perimeter of a rectangle
print()
print('# 1.9')
print()

print("Area =", 4.5 * 7.9)
print("Perimeter =", 2*(4.5 + 7.9))

# Convert km/s to mi/hr
print()
print('# 1.10')
print()

print(((14 / ((45 * 60) + 30)) * 60 * 60) / 1.6)

# Population projection
print()
print('# 1.11')
print()

print(312032486 + (((365 * 24 * 60 * 60) // 7) - ((365 * 24 * 60 * 60) // 13) + ((365 * 24 * 60 * 60) // 45)) * 1)
print(312032486 + (((365 * 24 * 60 * 60) // 7) - ((365 * 24 * 60 * 60) // 13) + ((365 * 24 * 60 * 60) // 45)) * 2)
print(312032486 + (((365 * 24 * 60 * 60) // 7) - ((365 * 24 * 60 * 60) // 13) + ((365 * 24 * 60 * 60) // 45)) * 3)
print(312032486 + (((365 * 24 * 60 * 60) // 7) - ((365 * 24 * 60 * 60) // 13) + ((365 * 24 * 60 * 60) // 45)) * 4)
print(312032486 + (((365 * 24 * 60 * 60) // 7) - ((365 * 24 * 60 * 60) // 13) + ((365 * 24 * 60 * 60) // 45)) * 5)
