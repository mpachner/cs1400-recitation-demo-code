# Week 6 - If Statements

An if statement creates a branch in your program's logic. This is useful for when you want certain code to execute only sometimes.
And if statement is laid out in the following form:
```python
if True: # if keyword, followed by a CONDITION (boolean) and the a colon :
   print("Hello World") # Things that happen inside the if statement must be indented
```

Conditions are statements that evaluate to True or False. '==' means 'is equal to'. '=' means 'is assigned to'

```python
a = True # a == True
b = 4 < 6 # b == True
c = 45 >= 60 # c == False
```

We can chain booleans together using AND, NOT, and OR. Not returns the opposite of a boolean (True=>False, False=>True).
Or returns True if any of the arguments are true. AND only returns true if both of the arguments are true.
With these 3 operators, you can construct any boolean expression. There are more complicated boolean expressions, but you do not need to know them.

```python
a = True # a == True
b = 4 < 6 # b == True
c = 45 >= 60 # c == False
d = a and b # d == True
e = a or b # e == True
f = d or c # f == True
g = not d # g == False
h = (c or e) and f # h == True
i = (c or not e) and f # i == False
```


We can use if statements to create different events for users. Look at the following example:
```python
name = eval(input("What is your name? :"))
if name == "Mason": # Use == for comparison, not =
    print("Welcome super user!")
```
Sometimes we want certain events to happen and not other events, we can separate events by using if/else:
Lines inside the if statement must have identical indents, and the else statement must have the same indent as the if
```python
hackerMode = False
name = eval(input("What is your name? :"))

if name == "Mason": # Use == for comparison, not =
    print("Welcome super user!")
    hackerMode = True
    print("Hacker mode activated!")
else: # Notice there is no condition on 
    print("Welcome regular user.")

if hackerMode: # Because hackerMode is a boolean(True/False) we can use it directly
    print("hack hack hack")
```

Sometimes we want certain events to happen but we have more than two cases. We can expand an if/else statement with elif:

```python
powerLevel = 0
name = eval(input("What is your name? :"))

if name == "Mason": # Use == for comparison, not =
    powerLevel = 100
# elif takes a condition, in this case 2 (Note that each comparison is separate. DO NOT do (name == "Chad Mano" or "Andy Brim")
elif name == "Chad Mano" or name == "Andy Brim": 
    powerLevel = 50
    print("Hello Professor")
elif name != "Bad User": # We could swap these last two blocks if we swapped the base condition
    powerLevel = 10
else:
    powerLevel = -10
print("Welcome,",name)

if powerLevel >= 50: # If the user has enough power, let them in
    print("hack hack hack")
else:
    print("You do not have enough power")
```

Pass. Pass is a keyword we can use in Python as a placeholder in a certain block. Pass does not do anything. It is just a marker for python that you will put something there later.
Pass will mostly just be used in notes as a placeholder. 

```python
## START OF BAD. This does not run
condition = 4 < 10
if condition:
    
#### END OF BAD
condition = 4 < 10
if condition:
    pass # The pass here makes the code compile
```

Nesting if statements.
"Nesting" a statement just means we are putting one inside the other.

```python
val = int(input("Enter an integer: "))
if val < 100:
    print("Your value is less than 100")
    if val < 50: # Nested if. 
        print("Your value is also less than 50")

```

## Case study 4.10

WRITE A SOFTWARE DEVELOPMENT PLAN. Using some cleverness you can write shorter and cleaner code than the book's example.

You can use nested if statements to write a program for computing taxes.

The United States federal personal income tax is calculated based on filing status and taxable income. There are four filing statuses: single filers, married filing jointly, married filing separately, and head of household. The tax rates vary every year. Table 4.2 shows the rates for 2009. If you are, say, single with a taxable income of $10,000, the first $8,350 is taxed at 10% and the other $1,650 is taxed at 15%. So, your tax is $1,082.50.

Table 4.2

```text
Marginal Tax Rate   Single                Married Filing Jointly   Married Filing Separately  Head of Household
10%                 $0 – $8,350           $0 – $16,700             $0 – $8,350	              $0 – $11,950
15%                 $8,351 – $33,950      $16,701 – $67,900        $8,351 – $33,950           $11,951 – $45,500
25%                 $33,951 – $82,250     $67,901 – $137,050       $33,951 – $68,525          $45,501 – $117,450
28%                 $82,251 – $171,550    $137,051 – $208,850      $68,526 – $104,425         $117,451 – $190,200
33%                 $171,551 – $372,950   $208,851 – $372,950      $104,426 – $186,475        $190,201 – $372,95
35%                 Anything else         Anything else            Anything else              Anything else
```
```python
status = eval(input())
if status == 0: 
    pass
    # Compute tax for single filers
elif status == 1: 
    pass
    # Compute tax for married filing jointly
elif status == 2: 
    pass
    # Compute tax for married filing separately
elif status == 3: 
    pass
    # Compute tax for head of household
else: 
    pass
    # Display wrong status
                 
```     
For each filing status there are six tax rates. Each rate is applied to a certain amount of taxable income. For example, of a taxable income of $400,000 for single filers, $8,350 is taxed at 10%, (33,950 – 8,350) at 15%, (82,250 – 33,950) at 25%, (171,550 – 82,250) at 28%, (372,950 – 171,550) at 33%, and (400,000 – 372,950) at 35%.


Listing 4.7 gives the solution to compute taxes for single filers. 
Listing 4.7 ComputeTax.py

You can look at my file: CaseStudy4_10.py for a complete answer.
```python
import sys

# Prompt the user to enter filing status
status = eval(input(
    "(0-single filer, 1-married jointly,\n" +
    "2-married separately, 3-head of household)\n" +
    "Enter the filing status: "))

# Prompt the user to enter taxable income
income = eval(input("Enter the taxable income: "))

# Compute tax
tax = 0

if status == 0:  # Compute tax for single filers 
    if income <= 8350:
        tax = income * 0.10
    elif income <= 33950:
        tax = 8350 * 0.10 + (income - 8350) * 0.15
    elif income <= 82250:
        tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + \
            (income - 33950) * 0.25
    elif income <= 171550:
        tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + \
            (82250 - 33950) * 0.25 + (income - 82250) * 0.28
    elif income <= 372950:
        tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + \
            (82250 - 33950) * 0.25 + (171550 - 82250) * 0.28 + \
            (income - 171550) * 0.33
    else:
        tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + \
            (82250 - 33950) * 0.25 + (171550 - 82250) * 0.28 + \
            (372950 - 171550) * 0.33 + (income - 372950) * 0.35
elif status == 1:  # Compute tax for married file jointly
    print("Left as exercise")
elif status == 2:  # Compute tax for married separately
    print("Left as exercise")
elif status == 3:  # Compute tax for head of household
    print("Left as exercise")
else:
    print("Error: invalid status")
    sys.exit()

# Display the result
print("Tax is", format(tax, ".2f"))
```