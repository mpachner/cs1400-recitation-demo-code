filingStatus = int(input("Filing status :"))
income = int(input("Income: "))
totalTaxes = 0
taxBracket = 0

ten = 8350
fifteen = 33950 - 8350
twentyFive = 82250 - 33950
twentyEight = 171550 - 82250
thirtyThree = 372950 - 171550

if filingStatus == 1:
    ten = 16700
    fifteen = 67900 - 16700
    twentyFive = 137050 - 67900
    twentyEight = 208850 - 137050
    thirtyThree = 372950 - 208850

if filingStatus == 2:
    ten = 8350
    fifteen = 33950 - 8350
    twentyFive = 68525 - 33950
    twentyEight = 104425 - 68525
    thirtyThree = 186475 - 104425

if filingStatus == 3:
    ten = 11950
    fifteen = 45500 - 11950
    twentyFive = 117450 - 45500
    twentyEight = 190200 - 117450
    thirtyThree = 372950 - 190200

if income > 0:  # 10 percent
    taxable = min(ten, income)
    totalTaxes += taxable * 0.1
    income -= taxable

if income > 0:  # 15 percent
    taxable = min(fifteen, income)
    totalTaxes += taxable * 0.15
    income -= taxable

if income > 0:  # 25 percent
    taxable = min(twentyFive, income)
    totalTaxes += taxable * 0.25
    income -= taxable

if income > 0:  # 28 percent
    taxable = min(twentyEight, income)
    totalTaxes += taxable * 0.28
    income -= taxable

if income > 0:  # 33 percent
    taxable = min(thirtyThree, income)
    totalTaxes += taxable * 0.33
    income -= taxable

if income > 0:  # 35 percent
    totalTaxes += income * 0.35

print("Total taxes:", totalTaxes)
