# Programming Exercise 6.29 (Page 209)
Credit card validator

## Functions and integer operations
In Week 4, we talked about mod and integer divide. We can use these tools with a base-10 number to find
digits and length.
```python
print(15 // 10) # 10 goes into 15 once
print(15 % 10) # 10 into 15 has 5 remainder
```
We can use this in a loop to print out each digit
```python
num = 47628
for _ in range(len(str(num))):
    print(num % 10)
    num //= 10
```
Output
```text
8
2
6
7
4
```
Following this logic, we can get the last 2 digits of a base-10 number by mod 100 and chop off two digits 
by integer dividing by 100.
```python
print(4234 % 100) # 34
print(4234 // 100) # 42
```
So finally, our formula for getting or cutting ```n``` digits is
```python
print(num % pow(10,n)) # print last n digits
print(num // pow(10,n)) # cutoff n digits
```

### More on Functions
Functions are made for code reuse. If you are doing something multiple times, put it in a function or loop.
To make your life easier, and to follow good coding practices, you should not have duplicate code in your project.
Consider the following bad example:
```python
import turtle
tr = turtle.Turtle()
tr.color("green")
tr.begin_fill()
tr.forward(10)
tr.right(90)
tr.forward(20)
tr.right(90)
tr.forward(10)
tr.right(90)
tr.forward(20)
tr.end_fill()
tr.goto(100,100)
tr.color("blue")
tr.begin_fill()
tr.forward(10)
tr.right(90)
tr.forward(20)
tr.right(90)
tr.forward(10)
tr.right(90)
tr.forward(20)
tr.end_fill()
``` 

This code works, but is hard to read and hard to maintain. Hard to maintain means maybe in the future we would like to
change the blue rectangle to be 100x400. There is a lot that has to change for that to happen. Maybe we also want to penup 
before we move to draw a rectangle, we would have to change that in every spot we made a rectangle.
A function fixes these problems. I also made a loop for the rectangle sides.
```python
import turtle
tr = turtle.Turtle()

def drawRectangle(tr,x,y,width,height,color):
    tr.color(color)
    tr.penup()
    tr.goto(x,y)
    tr.pendown()
    tr.begin_fill()
    for _ in range(2):
        tr.forward(width)
        tr.right(90)
        tr.forward(height)
        tr.right(90)
    tr.end_fill()

drawRectangle(tr,0,0,10,20,"blue")
drawRectangle(tr,100,100,10,20,"green")
```
Notice that I have passed the turtle into the function just like a normal argument. This will help you manage turtles over
multiple files. You'll understand more of why when we start Objects next week.
If we had multiple shapes we were drawing, we might want to create more functions.
```python
import turtle
tr = turtle.Turtle()

def gotoPenup(tr,x,y):
    tr.penup()
    tr.goto(x,y)
    tr.pendown()

def drawRightAngle(tr,first,second):
    tr.forward(first)
    tr.right(90)
    tr.forward(second)

def drawRectangle(tr,x,y,width,height,color,filled=False):
    gotoPenup(tr,x,y)
    tr.color(color)
    if filled:
        tr.begin_fill()
    for _ in range(2):
        drawRightAngle(tr,width,height)
        tr.right(90)
    if filled:
        tr.end_fill()

drawRectangle(tr,0,0,10,20,"blue",filled=True)
drawRectangle(tr,100,100,10,20,"green")
gotoPenup(tr,-100,100)
drawRightAngle(tr,100,200)
```