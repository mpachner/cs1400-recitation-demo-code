def main():
    number = eval(input("Enter a credit card number as a long integer: "))
    if isValid(number):
        print(number, "is valid")
    else:
        print(number, "is invalid")
    # Return true if the card number is valid


# Return the number of digits in d
def getSize(d):
    # This is the mathematical way of solving the problem
    # numberOfDigits = 0
    # while d != 0:
    #     numberOfDigits += 1
    #     d = d // 10
    return len(str(d))  # This is the hacky way


# We can chain the whole expression into one boolean
def isValid(number):
    return getSize(number) >= 13 and getSize(number) <= 16 and (
            prefixMatched(number, 4) or prefixMatched(number, 5) or prefixMatched(number, 6) or prefixMatched(
        number, 37)) and (sumOfDoubleEvenPlace(number) + sumOfOddPlace(number)) % 10 == 0


# 1253 => getDigit(5*2) + getDigit(1*2) = 1 + 2 = 3
def sumOfDoubleEvenPlace(number):
    result = 0
    number //= 10  # Starting from the second digit from left
    while number != 0:
        result += getDigit((number % 10) * 2)
        number //= 100  # Move to the next even place
    return result


# Return this number if it is a single digit, otherwise, return
# the sum of the two digits
# 4 => 4
# 14 => 1 + 4 = 5
def getDigit(number):
    return number % 10 + (number // 10)


# Return sum of odd place digits in number
# 23467 => 7+ 4 + 2 = 13
# 124 => 4 + 1 = 5
def sumOfOddPlace(number):
    result = 0
    while number != 0:
        result += number % 10
        number //= 100  # Move two positions to the left
    return result


# Return true if the digit d is a prefix for number
# 123678, 123 -> True
# 1278364, 128 -> False
def prefixMatched(number, d):
    return getPrefix(number, getSize(d)) == d


# Return the first k number of digits from number. If the
# number of digits in number is less than k, return number.
# 1234,2 -> 12
# 213479,4 -> 2134
def getPrefix(number, k) -> int:
    cutoff = getSize(number) - k
    return number // (10 ** cutoff)


main()
