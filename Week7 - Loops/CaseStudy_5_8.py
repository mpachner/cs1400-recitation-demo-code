# Case study 5.8 (Page 154)

from math import sqrt

TOTAL_PRIMES = 50
primeCount = 0

PER_ROW = 10

currentPrime = 2

while True:
    isPrime = True
    for divisor in range(2, int(sqrt(currentPrime)) + 1):
        if currentPrime % divisor == 0:
            isPrime = False
            break
    if isPrime:
        print(currentPrime, end=" ")
        primeCount += 1
        if primeCount % PER_ROW == 0:
            print()
    if primeCount == TOTAL_PRIMES:
        break
    currentPrime += 1
