# Week 7 - While loops

### Case study 5.8 (Page 154)
Calculating primes

Math thought: When calculating the factors
of a number, we can go up to the square root of the number if we calculate two divisors at the same time.
How does this work?
What are the advantages / disadvantages of this method? Would a different one work better?


### Programming Exercise 5.26 (Page 163)
Sum of a series

## Loops

Loops are ways of doing many similar operations without having to write out all cases.
There are two main types of loops ```while``` and ```for```. A loop is similar to an if statement,
the inside code only happens if the condition is true. The only difference is once the inside code runs,
python checks to see if the condition is still true, and if it is, executes the loop again.

We call one loop run through an "Iteration"


Example, if versus while:

If statement:
```python
counter = 0
if counter < 5:
    print("Counter is:",counter)
    counter += 1

print("Final counter is:",counter)
```
Output
```text
Counter is: 0
Final Counter is: 1
```
Versus a while loop:
```python
counter = 0
while counter < 5:
    print("Counter is:",counter)
    counter += 1

print("Final counter is:",counter)
```
Output
```text
Counter is: 0
Counter is: 1
Counter is: 2
Counter is: 3
Counter is: 4
Final Counter is: 5
```

The only difference is that the while loop checks to see if it is still true. If you mess up the condition, this could
present problems

Bad Example:
```python
counter = 0
while counter < 5:
    print("Counter is:",counter)
    counter -= 1

# This line never happens because the loop condition is always true!
print("Final counter is:",counter)
```
Output (INFINITE)
```text
Counter is: 0
Counter is: -1
Counter is: -2
Counter is: -3
....
```

A for loop can help us not create infinite loops. For now, we will use a special function called range() for our for loops.
Range by default begins at 0. Range is a [begin,end) set (begin is inclusive, end is exclusive). 
```python
a = range(5) # 0,1,2,3,4.
a = range(0,5) # 0,1,2,3,4.
```

```python
for counter in range(5):
    print("Counter is:",counter)
```
Output
```text
Counter is: 0
Counter is: 1
Counter is: 2
Counter is: 3
Counter is: 4
```

We can also put a "step" on our range to change which direction our range goes, or how fast it goes
Remember that range is a close-open set.

```python
a = range(0,5,1) # 0,1,2,3,4.
a = range(0,5,2) # 0,2,4 # The next number,6, is too large
a = range(0,6,2) # 0,2,4 # The next number,6, is still too large
a = range(0,7,2) # 0,2,4,6 # 6 is now in the range
a = range(5,0,-1) # 5,4,3,2,1 # start > end, but we move backwards
a = range(4,1,-1) # 4,3,2,1,0 # to get the same list as the start but backwards
```

I would advise using a for loop when possible. The general rule is this: If you know how many loops you want to complete,
use a for loop. If you don't know, use a while loop. When looping through user input, generally use a while loop because
you do not know how many times they are going to interact with your system.

## Special loop keywords
Loops have two special keywords to help control the loop flow if necessary. ```break``` and ```continue```

Break instantly stops the most recent loop:

```python
while True:
    response = input("Guess my secret password: ")
    if response == "password":
        break
print("Access granted")
```
Example Input/Output
```text
Guess my secret password: pickles
Guess my secret password: oink
Guess my secret password: password
Access granted
```

Continue instantly goes to the next iteration of the most recent loop:

```python
noNoNumber = 6
for i in range(4,8):
    print(i)
    if i == noNoNumber:
        continue
    print(i, "is a good number")
```
Output
```text
4
4 is a good number
5
5 is a good number
6
7
7 is a good number
```

These get a bit more complicated in nested loops

```python
while True:
    total = 0
    num = int(input("Enter an integer: "))
    # This if continues the while loop
    if num % 7 == 0:
        continue
    for i in range(0,num):
        # This if continues the for loop
        if i == 0:
            continue
        total += num//i
        # This if breaks us out of the for loop
        if total == 20:
            break
    # This if breaks us out of the while loop
    if total == 20:
        break
```

Without a break in the outer loop, it would go on forever.