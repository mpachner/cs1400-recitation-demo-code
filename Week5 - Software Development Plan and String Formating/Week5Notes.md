# Week 5 - Software Development Plan and String Formatting. 


## Section 2.13 (Page 48)
- The user enters two points. Find and display the distance between the points in the x direction, y direction, and the total (direct from point to point). 
Use the math module importing the module and again importing just specific functions.
 All distances should be displayed as positive numbers. 
  - The two import methods you should use are
    - ```import <module>```
    - ```from <module> import <function>```
- Writing a Software Development Plan(SDP) (Page 48-50)
  - Create a file named planX.txt (Where X is the task number, (plan1.txt etc.))
  - Step 1 (Requirements Specification)
    - What is the purpose of the program?
  - Step 2 (System Analysis)
    - Identify input and output, how to get output
  - Step 3 (System Design)
    - Identify the steps required to complete the problem
  - Step 5 (Test cases)
    - Create at least 2 example inputs and the expected output for each
  - Step 4 (Code)
    - Code. Nothing special. You don't have to do anything for this section in the SDP

## Exercises 3.17 -> 3.24 (Page 80)
3.19 -> 3.24 are given to you with one print statement per line. Using the augmented operator ```+=``` change 3.19->3.24 
to use only one print statement. Observe what the formatting does, you will need to know it for the test.
```python
output = ...
output += ...
print(output)
```

#### Useful notes from the book
```text
"10.2f"	       Format the float item with width 10 and precision 2.
"10.2e"	       Format the float item in scientific notation with width 10 and precision 2.
"5d"	       Format the integer item in decimal with width 5.
"5x"	       Format the integer item in hexadecimal with width 5.
"5o"	       Format the integer item in octal with width 5.
"5b"	       Format the integer item in binary with width 5.
"10.2%"	       Format the number in decimal.
"50s"	       Format the string item with width 50.
"<10.2f”       Left-justify the formatted item.
">10.2f"       Right-justify the formatted item.
```

#### 3.17 What is the return value from invoking the format function?

#### 3.18 What happens if the size of the actual item is greater than the width in the format specifier?

#### 3.19 Show the printout of the following statements:

```python
print(format(57.467657, "9.3f"))
print(format(12345678.923, "9.1f"))
print(format(57.4, ".2f"))
print(format(57.4, "10.2f"))
```

#### 3.20 Show the printout of the following statements:
```python
print(format(57.467657, "9.3e"))
print(format(12345678.923, "9.1e"))
print(format(57.4, ".2e"))
print(format(57.4, "10.2e"))
```

#### 3.21 Show the printout of the following statements:
```python
print(format(5789.467657, "9.3f"))
print(format(5789.467657, ">9.3f"))
print(format(5789.4, ".2f"))
print(format(5789.4, "<.2f"))
print(format(5789.4, ">9.2f"))
```

#### 3.22 Show the printout of the following statements:
```python
print(format(0.457467657, "9.3%"))
print(format(0.457467657, ">9.3%"))
```

#### 3.23 Show the printout of the following statements:
```python
print(format(45, "5d"))
print(format(45, ">5d"))
print(format(45, "5x"))
print(format(45, ">5x"))
```

#### 3.24 Show the printout of the following statements:
```python
print(format("Programming is fun", "25s"))
print(format("Programming is fun", ">25s"))
print(format("Programming is fun", ">25s"))
```







